#!/bin/bash

set -eu

cd $HOME
sudo yum install git
if [[ ! -d ./my-dotfiles ]]; then
	git clone https://twabiko@bitbucket.org/twabiko/my-dotfiles
fi
python ./my-dotfiles/setup.py

if [[ ! -d ~/vimfiles/bundle/Vundle.vim ]]; then
	git clone https://github.com/VundleVim/Vundle.vim.git ~/vimfiles/bundle/Vundle.vim
fi

# 初回 vim 実行時にキー入力が発生することを避けたい。
# そのため Vundle に頼らず、手動で git clone する。
#vim +PluginInstall +qall
pushd /home/wabiko/my-dotfiles/vimfiles/bundle
cat $HOME/vimfiles/vimrc/plugins.vim \
    | grep ^Plugin \
    | sed -e "s#^.*'\(.*\)/\(.*\)'#\1 \2#" \
    | while read -r user repo; do
        url="https://github.com/${user}/${repo}"
        if [[ ! -d $repo ]]; then
            git clone $url
	fi
      done
popd >/dev/null

pushd /home/wabiko/my-dotfiles/vimfiles/bundle/ansible-vim/UltiSnips >/dev/null
which ansible >/dev/null || sudo yum install --disablerepo=\* --enablerepo={base,updates,extras} ansible
./generate.py
popd >/dev/null
