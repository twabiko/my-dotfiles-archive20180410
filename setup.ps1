# setup.ps1

# このスクリプト実行は Symlink の扱いのため PowerShell v5 以降必須
if ($PSVersionTable.PSVersion.Major -lt 5) {
  exit 1
}

cd $ENV:userprofile

function my_mklink($link, $target_file_or_directory) {
  if (-not (Test-Path -Path $link)) {
    New-Item -Name $link -ItemType SymbolicLink -Value $target_file_or_directory
  }
}

my_mklink .curlrc           my-dotfiles\dot.curlrc
my_mklink .editorconfig     my-dotfiles\dot.editorconfig
my_mklink .env.sample       my-dotfiles\dot.env.sample
my_mklink .gitconfig        my-dotfiles\dot.gitconfig
my_mklink .gitconfig.sample my-dotfiles\dot.gitconfig.sample
my_mklink .hgrc             my-dotfiles\dot.hgrc
my_mklink .hgrc.sample      my-dotfiles\dot.hgrc.sample
my_mklink .vimrc            my-dotfiles\dot.vimrc
my_mklink vimfiles          my-dotfiles\vimfiles
my_mklink .wgetrc.sample    my-dotfiles\dot.wgetrc.sample
#my_mklink templates         my-dotfiles\templates #FIXME: fail because Windows system has Templates special directory

# vim:ts=2 sw=2 sts=2 et:
