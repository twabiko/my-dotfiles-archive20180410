#!/bin/bash

set -eu

option_absolute_path=n
option_mode=n
option_sudo=n

function usage {
    echo "テキストファイルを copy & paste で複製できるよう出力するツールです" 1>&2
    echo "" 1>&2
    echo "usage: $(basename $0) [options] path..." 1>&2
    echo "example: ./$(basename $0) /etc/hosts" 1>&2
    echo "" 1>&2
    echo "-a: use absolute path" 1>&2
    echo "-m: use chmod to preserve mode" 1>&2
    echo "-s: use sudo to preserver owner" 1>&2
    exit 1
}

function path_to_echo {
    local path="$1"

    local fname="$(basename "$path")"

    local q="'"
    local qq='"'

    # 必要なら sudo コマンドを追加する
    local sudo=""
    if [[ $option_sudo == y ]]; then
        local user=$(stat --printf "%U" "$path")
        sudo="sudo -u $user "
    fi

    # 必要なら絶対パスにする
    if [[ $option_absolute_path == y ]]; then
        fname=$(readlink -f "$1")
    fi

    # 出力する
    echo "("
    $sudo cat "$path" | while IFS= read -r line; do
        echo "  echo '${line//$q/$q$qq$q$qq$q}'"
    done
    printf ") | %stee %s >/dev/null\n" "$sudo" "$fname"

    if [[ $option_mode == y ]]; then
        local mode=$(stat --printf "%a" "$path")
        printf "%schmod %s %s\n" "$sudo" "$mode" "$fname"
    fi
}

# 引数を解析する
while getopts ahms OPT; do
    case $OPT in
        a)  option_absolute_path=y
            ;;
        h)  usage
            ;;
        m)  option_mode=y
            ;;
        s)  option_sudo=y
            ;;
        \?) usage
            ;;
    esac
done
shift $((OPTIND - 1))

# パスが１つもなければ異常終了とする
if [[ $# -eq 0 ]]; then
    usage
fi

# すべてのパスを処理する
while [[ $# -gt 0 ]]; do
    if [[ -f "$1" ]]; then
        path_to_echo "$1"
    else
        echo "error: \"$1\" is not file" 1>&2
        exit 1
    fi
    shift 1
done
