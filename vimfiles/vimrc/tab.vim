" .vim/vimrc/tab.vim

" ----------------------------------------
" 色数

if !has('gui_running')
    " 色数は 256 を指定
    set t_Co=256
endif


" ----------------------------------------
" 画面全体

" 無音のために visualbell を有効にしつつフラッシュを無効に。
" Windows では _gvimrc に記述する必要あり。
set visualbell
set t_vb=

if has('gui_running')
    " Windows の gvim 環境では、
    " フラッシュ抑制に novisualbell が必要だったので追加
    set novisualbell
endif

" ----------------------------------------
" 最下行

" 常にステータスを表示させる
set laststatus=2


" ----------------------------------------
" 左端

" GUIであれば行番号を表示させる
if has('gui_running')
    set number
else
    set nonumber
endif


" ----------------------------------------
" 最下行以外

" 文法による強調表示を有効にする
syntax on

" 検索キーワードを強調させない
set nohlsearch

" 常に先頭・末尾に指定行数を表示するようスクロールする
set scrolloff=5

" 折り畳み行の行頭に視覚的なインデントを入れる
if v:version > 704
    set breakindent
endif


" ----------------------------------------
" カーソル行

" カーソル行を表示させる
set cursorline

" ファイルを開いたら、カーソルがあった場所に移動する
augroup vimrcEx
    autocmd BufRead * if line("'\"") > 0 && line("'\"") <= line("$") |
        \ exe "normal g`\"" | endif
augroup END

