" .vim/vimrc/functions.vim

function! MyRestructuredText()
    set ft=rst sw=2 sts=2 ts=2
endfunction

function! MyMarkdown()
    set ft=markdown sw=2 sts=2 ts=2
endfunction
