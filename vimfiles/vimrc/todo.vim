" todo.vim

" "tli " で "* []" を入力 (Todo List Itemの略)
abbreviate tli * [ ]

" Normal モードで "\\" タイプで todo のon/offを切替え
" BUG: なんらかの動作後に無効化されてる気がする。
nnoremap <buffer> <Leader><Leader> :call MyToggleCheckbox()<CR>

function! MyToggleCheckbox()
  let l:line = getline('.')
  if l:line =~ '^\s*\* \[ \]'
    let l:result = substitute(l:line, '^\(\s*\)\* \[ \]', '\1* [x]', '')
    call setline('.', l:result)
  elseif l:line =~ '^\s*\* \[x\]'
    let l:result = substitute(l:line, '^\(\s*\)\* \[x\]', '\1* [ ]', '')
    call setline('.', l:result)
  end
endfunction

