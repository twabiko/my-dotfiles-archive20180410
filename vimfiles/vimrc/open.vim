" .vim/vimrc/open.vim

" 既存ファイルを編集するときのエンコーディング候補
set fileencodings=utf-8,cp932

" 各ファイルタイプごとの設定は ~/.editorconfig を更新すること
"autocmd BufRead,BufNewFile Vagrantfile setlocal sw=2 sts=2 ts=2 et
