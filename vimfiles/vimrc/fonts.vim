" .vim/vimrc/fonts.vim

" :MyNormalFontSize でフォントサイズを普通にする
command! MyNormalFontSize call MyNormalFont()
function! MyNormalFontSize()
    set guifont=Cica:h12:cSHIFTJIS:qDRAFT,Ricty_Diminished:h12:cSHIFTJIS:qDRAFT,Migu_1M:h12:cSHIFTJIS:qDRAFT
endfunction

" :MyLargeFontSize でフォントサイズを大きくする
command! MyLargeFontSize call MyLargeFont()
function! MyLargeFontSize()
    set guifont=Cica:h20:cSHIFTJIS:qDRAFT,Ricty_Diminished:h20:cSHIFTJIS:qDRAFT,Migu_1M:h20:cSHIFTJIS:qDRAFT
endfunction

" :MyExtraLargeFontSize でフォントサイズを特大する
command! MyExtraLargeFontSize call MyExtraLargeFont()
function! MyExtraLargeFontSize()
    set guifont=Cica:h28:cSHIFTJIS:qDRAFT,Ricty_Diminished:h28:cSHIFTJIS:qDRAFT,Migu_1M:h28:cSHIFTJIS:qDRAFT
endfunction

" フォントを設定する
call MyNormalFontSize()

if has('gui_running')
    " プリンタフォント指定。これがないと文字化けしたので追加。
    set guifont=Cica:h12:cSHIFTJIS,Ricty_Diminished:h12:cSHIFTJIS,Migu_1M:h12:cSHIFTJIS
endif
