" .vim/vimrc/im.vim

" insert モードで IM を OFF にする
set iminsert=0

" 検索するとき、 IM を iminsert 設定と同じ設定にする
set imsearch=-1
