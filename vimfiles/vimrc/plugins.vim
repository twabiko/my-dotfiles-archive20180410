" vimfiles/vimrc/plugins.vim

" TODO: 利用しそうな環境がすべて vim8 になったら vim8 標準のプラグインマネージャを検討する。ただし、CentOS7 は vim 7 なので当面先。

" プラグインインストール方法:
" $ git clone https://github.com/VundleVim/Vundle.vim.git ~/vimfiles/bundle/Vundle.vim
" $ vim
" :PluginInstall
"
" vim 起動中であれば、次のコマンド実行でも導入できる
" :source ~/.vimrc
" :PluginInstall

" ----------------------------------------
" Vundle のセットアップ手順に則る
set nocompatible " be iMproved, required
filetype off " required
if has('gui_running')
    set rtp+=$HOME/vimfiles/bundle/Vundle.vim/
    call vundle#begin($USERPROFILE.'/vimfiles/bundle/')
else
    set rtp+=~/vimfiles/bundle/Vundle.vim
    call vundle#begin('~/vimfiles/bundle')
endif
Plugin 'VundleVim/Vundle.vim'
" 参考: https://github.com/VundleVim/Vundle.vim

" ----------------------------------------
" hybrid.vim - A dark color scheme for Vim
Plugin 'w0ng/vim-hybrid'

" 使い方
" :colorscheme hybrid

" ----------------------------------------
" The main focus when developing gruvbox is to keep colors easily distinguishable, contrast enough and still pleasant for the eyes.
Plugin 'morhetz/gruvbox'

" 使い方
" :colorscheme gruvbox

" ----------------------------------------
" A dark, low-contrast, Vim colorscheme.
Plugin 'romainl/Apprentice'

" 使い方
" :colorscheme apprentice

" ----------------------------------------
" *submode.txt* Create your own submodes
Plugin 'kana/vim-submode'

" call submode#xxx 定義で動きを変えられる。

" Ctrl+w + 等を繰り返し入力せずにサイズ変更できる関数を定義しておく。
" vundle#end() 実行後に呼び出す。Plugin直後だとエラーとなるため。
function! MySubmode()
    call submode#enter_with('winsize', 'n', '', '<C-w>>', '<C-w>>')
    call submode#enter_with('winsize', 'n', '', '<C-w><', '<C-w><')
    call submode#enter_with('winsize', 'n', '', '<C-w>+', '<C-w>+')
    call submode#enter_with('winsize', 'n', '', '<C-w>-', '<C-w>-')
    call submode#map('winsize', 'n', '', '>', '<C-w>>')
    call submode#map('winsize', 'n', '', '<', '<C-w><')
    call submode#map('winsize', 'n', '', '+', '<C-w>+')
    call submode#map('winsize', 'n', '', '-', '<C-w>-')
endfunction

" 使い方
" vim
" :split <enter>
" :vsplit <enter>
" Ctrl-w +++ <esc>
" Ctrl-w --- <esc>
" Ctrl-w >>> <esc>
" Ctrl-w <<< <esc>

" ----------------------------------------
" NERDTree

Plugin 'scrooloose/nerdtree'

" 開始時にブックマークを表示
let NERDTreeShowBookmarks = 1

" 隠しファイルを表示
let NERDTreeShowHidden = 1

" 隠しファイルは先頭に表示
let NERDTreeSortHiddenFirst = 1

set encoding=utf8
" 次の指定をする場合、端末フォントは Cica が望ましい。
" utf8 を指定する必要がある。
source $HOME/vimfiles/vimrc/nerdtreedirallow.vim

" 使い方
" :NERDTree でツリーエクスプローラーを開く。

" ----------------------------------------
" vimfiler - A powerful file explorer implemented in Vim script

Plugin 'Shougo/vimfiler.vim'

" 使い方
" :VimFiler

" ----------------------------------------
" The unite or unite.vim plug-in can search and display information from arbitrary sources like files, buffers, recently used files or registers.

Plugin 'Shougo/unite.vim'

" 使い方
" :Unite file buffer

" ----------------------------------------
" ctrlp.vim - Full path fuzzy file, buffer, mru, tag, ... finder for Vim.

Plugin 'ctrlpvim/ctrlp.vim'

" 使い方
" :CtrlP<enter> の後で、編集したいファイル名の一部を入力する

" ----------------------------------------
" VimDevIcons - Add Icons to Your Plugins

" PuTTY の場合、次の設定を行うこと。
" でないと、アイコンが半分のサイズで表示されてしまう。
" Window > Translation > [x] Treat CJK ambiguous characters as wide
Plugin 'ryanoasis/vim-devicons'
set encoding=utf8

" encoding=utf8 かつメニューを日本語で表示しようとしたとき、表示がおかしくなる事象の対処として、英語表示とする。
set langmenu=en_US
source $VIMRUNTIME/delmenu.vim
source $VIMRUNTIME/menu.vim

set ambiwidth=double

" フォルダアイコンの表示をON
let g:WebDevIconsUnicodeDecorateFolderNodes = 1

" ----------------------------------------
" vim-nerdtree-syntax-highlight - This adds syntax for nerdtree on most common file extensions.

Plugin 'tiagofumo/vim-nerdtree-syntax-highlight'

" ----------------------------------------
" BufExplorer Plugin for Vim

Plugin 'jlanzarotta/bufexplorer'

" 使い方
" :BufExplorer で直近のバッファを表示

" ----------------------------------------
" lightline.vim - A light and configurable statusline/tabline plugin for Vim
Plugin 'itchyny/lightline.vim'

source $HOME/vimfiles/vimrc/lightline.vim

" ----------------------------------------
" Indent Guides is a plugin for visually displaying indent levels in Vim.
Plugin 'nathanaelkane/vim-indent-guides'

" インデントガイドをデフォルトで有効にする
let g:indent_guides_enable_on_vim_startup = 1

" colorscheme 未設定時に動作しないので設定した
colorscheme default

" ガイドのサイズは 1 文字で表現
let g:indent_guides_guide_size = 1

" nerdtree はインデントから除外
let g:indent_guides_exclude_filetypes = ['help', 'nerdtree']

" ----------------------------------------
" EditorConfig Vim Plugin

" Windows 環境では、別途 Python を導入する必要がある
Plugin 'editorconfig/editorconfig-vim'

" 使い方
" http://editorconfig.org/ を参照。

" ----------------------------------------
" quickrun.txt - Run a command and show its result quickly.

Plugin 'thinca/vim-quickrun'

" 使い方
" :QuickRun<enter> でバッファ内を実行すると結果が別バッファで得られる。
" Python 等のコードを書いて実行する等が便利。

" ----------------------------------------
" C-c C-c で編集中行か選択中の複数行を tmux 経由で別プロセスに渡す
if !has('win32')
    Plugin 'jpalardy/vim-slime'
endif

" 使い方:
" $ tmux
" C-b :split-window
"   --> 画面が水平2分割される
" C-b q
"   --> 画面の番号を確認
" vim
" idate<ESC>
"   --> 1行目に date を書く
" C-c C-c
" tmux socket name: default
" tmux target pane: :0.0
"   --> 初回はどの pane に出力するか決める
"   --> date が実行される
" C-c C-c
"   --> 2回目以降は pane 指定不要となる

if !has('win32')
    " slime のターゲットを tmux とする
    let g:slime_target = "tmux"
endif

" ----------------------------------------
" Surround.vim is all about "surroundings": parentheses, brackets, quotes, XML tags, and more.
Plugin 'tpope/vim-surround'

" Visualモードで範囲を選択して S + '記号' と入力すると、記号で括る
" vim
" iThis is a selected text.<ESC>
" /a<enter>
" v/t<enter>nn
" S"

" ----------------------------------------
" fugitive.vim may very well be the best Git wrapper of all time.
Plugin 'tpope/vim-fugitive'

" :Greview で差分を出力する
command! Greview :Git! diff --staged

" コミットログを確認する
"   :Glog | copen
"     --> quickfix にログを表示
" キーワード検索する
"   :Ggrep 検索したいワード | copen
"     --> quickfix に検索結果を表示
" 編集箇所がどのコミットか確認する
"   :Gblame
" コミットするファイルを選ぶ
"   :Gstatus
"     --> 行選択して、 - で add/reset に。 p でパッチ表示。
" コミット予定の差分を確認する
"   :Git! diff --cached
"     --> git diff --cached
"   :Greview
" コミットする
"   :Gcommit
" プッシュする
"   :Gpush

" ----------------------------------------
" It provides nice syntax coloring and indenting for Windows PowerShell (.ps1) files, and also includes a filetype plugin so Vim can autodetect your PS1 scripts.
Plugin 'PProvost/vim-ps1'

" --> *.ps1 文法を解釈できるようになり配色がきれいになる
" --> 編集時の単語境界にハイフン'-'が入るので
"     Write-Host 等、PowerShell 的な補完が行いやすくなる

" ----------------------------------------
" This repository contains snippets files for various programming languages.
Plugin 'honza/vim-snippets'

" ----------------------------------------
" UltiSnips is the ultimate solution for snippets in Vim.
Plugin 'SirVer/ultisnips'

" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" 使い方:
" $ vim hoge.c
" main<tab>
"   --> main 関数のひな型作られる
" $ vim hoge.yml
" :set ft=ansible
" lineinfile<tab>

" 他にもスニペット管理があるが、
" ansible-vim のディレクトリ名にあわせて UltiSnips を選択した。

" ----------------------------------------
" A vim plugin for syntax highlighting Ansible's common filetypes
Plugin 'pearofducks/ansible-vim'

au BufRead,BufNewFile */playbooks/*.yml set filetype=ansible
au BufRead,BufNewFile */ansible/*.yml set filetype=ansible

" 初期設定:
" $ sudo yum install epel-release -y
" $ sudo yum-config-manager --disable epel
" $ sudo yum --enablerepo=epel install ansible -y
" $ cd ~/vimfiles/bundle/ansible-vim/UltiSnips
" $ ./generate.py

" 使い方:
" $ mkdir -pv /tmp/playbooks
" $ vim /tmp/playbooks/hoge.yml
" i- name: hoge<enter>lineinfile<tab>
"   --> lineinfile の引数が列挙される

" ----------------------------------------
" Syntax highlighting, matching rules and mappings for the original Markdown and extensions.

" 使い方:
" $ echo "<!-- vim:set ft=markdown: -->" > newfile.txt
" $ vim newfile.txt

Plugin 'plasticboy/vim-markdown'

let g:vim_markdown_folding_disabled = 1

" ----------------------------------------
call vundle#end()            " required
filetype plugin indent on    " required

" Plugin直後で定義するとエラーとなったが、ココならうまく動いた。
call MySubmode()

"colorscheme hybrid
" 次の理由で追加した:
" * PuTTY + vim で綺麗に見せるために必要なため追加した
" * Windows + gvim で :source ~/.vimrc を実行したとき、
"   背景色がおかしくなるため、追加した
"set background=dark

"colorscheme gruvbox " 2017/12/30 試行中 →目に優しくない
colorscheme apprentice " 2017/12/30 試行中 →hybrid より冷たい印象でステキ

" ----------------------------------------
" 試したいが試してない

" davidhalter/jedi-vim; Python補完
" pyflakes-vim; 構文チェック
" vim-flake8; 文法チェック

" ----------------------------------------
" 試したが、あまり使わず、使わないことにしたプラグイン

"Plugin 'jonathanfilip/vim-lucius'
"Plugin 'tomasr/molokai'
"Plugin 'gosukiwi/vim-atom-dark'
"Plugin 'raphamorim/lucario'
"Plugin 'jdkanani/vim-material-theme'
"Plugin 'jacoborus/tender.vim'
"Plugin 'godlygeek/tabular'

" tigris.nvim " JavaScript 構文解析

" ----------------------------------------
" 試したが、使うことを避けたいと感じたプラグイン

" j, k を継続で押すと移動が加速する
" 止まるまでに時間がかかるので、止めたい場所で止めにくい。
"Plugin 'rhysd/accelerated-jk'
"nmap j <Plug>(accelerated_jk_gj)
"nmap k <Plug>(accelerated_jk_gk)

" :source ~/.vimrc で :NERDTree の表示がおかしくなるため、次の行を追加
" https://github.com/ryanoasis/vim-devicons/wiki/FAQ-&-Troubleshooting#how-do-i-solve-issues-after-re-sourcing-my-vimrc
if exists("g:loaded_webdevicons")
    call webdevicons#refresh()
endif

