" .vim/vimrc/tmp.vim

" スワップファイル (*.swp) は作成する (二重編集防止のため)
set swapfile

" バックアップファイル (*~) を作らない
set nobackup

" undo ファイル (un~) を作らない
set noundofile

