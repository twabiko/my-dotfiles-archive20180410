" .vim/vimrc/gui.vim

if has('win32')
    " 選択範囲をクリップボードと連携させる
    set guioptions+=a

    " ツールバーを非表示にする
    set guioptions-=T
endif

