" .vim/vimrc/commands.vim

" :MyRest で .rst 用設定にする
command! MyRest call MyRestructuredText()

" :MyMarkdown で .md 用設定にする
command! MyMarkdown call MyMarkdown()
