# .bashrc.d/20-ruby-rbenv.sh

# check rbenv is installed
which rbenv >/dev/null 2>&1
if [ $? -eq 0 ]; then
    export PATH="$HOME/.rbenv/shims:$PATH"
    eval "$(rbenv init -)"
fi
