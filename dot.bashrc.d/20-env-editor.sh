# .bashrc.d/10-env-editor.sh

# crontab -e, visudo 等で使うエディターを指定する
which vim >/dev/null && EDITOR=vim
: ${EDITOR:=vi}

export EDITOR
