このリポジトリは、 Windows, Linux で割とよく使う環境設定を集約したもの。

Linux導入
---------

::

  curl -s https://bitbucket.org/twabiko/my-dotfiles/raw/master/setup.sh | tee /tmp/setup.sh
  chmod +x /tmp/setup.sh
  /tmp/setup.sh

または

::

  cd ~
  sudo yum install git
  git clone https://twabiko@bitbucket.org/twabiko/my-dotfiles
  python ./my-dotfiles/setup.py

  # vim を設定する
  git clone https://github.com/VundleVim/Vundle.vim.git ~/vimfiles/bundle/Vundle.vim
  vim +PluginInstall +qall

  sudo yum install --disablerepo=\* --enablerepo={base,updates,extras} ansible
  pushd /home/wabiko/my-dotfiles/vimfiles/bundle/ansible-vim/UltiSnips
  ./generate.py
  popd

Windows導入
-----------

::

  # cmd.exe を管理者権限で実行
  cd %userprofile%
  choco install git
  git clone https://twabiko@bitbucket.org/twabiko/my-dotfiles
  powershell .\my-dotfiles\setup.ps1

  # vim を設定する
  git clone https://github.com/VundleVim/Vundle.vim.git ~/vimfiles/bundle/Vundle.vim
  vim
  :PluginInstall

  cd %USERPROFILE%\vimfiles\bundle\ansible-vim\UltiSnips
  .\generate.py

更新
----

::

  cd %USERPROFILE%\my-dotfiles
  git status
  git pull

