# filename: make-rst-template.ps1

# タイトルをもとに、日付付のファイル名で *.rst ファイルのテンプレを生成
# 例: C:\Users\user\Desktop\yyyymmdd_title.txt

Set-StrictMode -Version Latest

# テキストボックスと、OK, Cancel ボタンを表示する
function InputBox
{
  param($res = '')
  [void][Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic')
  $title = 'Make RestructuredText Template'
  $msg   = 'Enter Title:'
  $text = [Microsoft.VisualBasic.Interaction]::InputBox($msg, $title, $res)
  if ($text -eq '') {
    return $null # '' or Cancel Button
  }
  return $text
}

# デスクトップのパスを返す
function GetDesktopPath
{
  return [Environment]::GetFolderPath('Desktop')
}

# cp932 でバイト数を返す
function GetSjisByteCount
{
  param($str)
  $n = [System.Text.Encoding]::GetEncoding("shift_jis").GetByteCount($str)
  return $n
}

# 半角文字を1、全角文字を2として、文字数を返す
function MyStrLength
{
  param($str)
  return GetSjisByteCount($str)
}

# ファイル名を決定する
function GetBasename
{
  param($rest_title)

  $basename = $rest_title

  # NTFSで使用NGな文字 \/:,;*?"<>| は消す。
  $basename = $basename -replace '[\/:,;*?"<>|]','X'

  # 半角スペースはアンダースコアに変換しておく。
  # コマンドプロンプト等でダブルクォートで括る必要があるため。
  $basename = $basename -replace ' ','_'

  return $basename
}

# Rest のタイトルを決定する
function GetRestTitle
{
  param($title)
  $prefix = Get-Date -Format "yyyyMMdd "
  $rest_title = $prefix + $title
  return $rest_title
}

# アンダーラインを生成する
function GetUnderline {
  param($str)
  $n = GetSjisByteCount($str)
  $ret = ''
  for ($i = 0; $i -lt $n; $i++) {
    $ret = $ret + "="
  }
  return $ret
}

$title = InputBox('')
if ($title -ne $null) {
  $rest_title = GetRestTitle($title)
  $basename = GetBasename($rest_title)
  $dir = GetDesktopPath
  $fullpath = $dir + "\" + $basename + ".txt"
  $fullpath
  $underline = GetUnderline($rest_title)
  $body = @(
    $rest_title,
    $underline,
    "",
    ".. vim:ft=rst:"
  )
  $body | Out-File $fullpath -Encoding UTF8 -NoClobber
  Invoke-Item $fullpath
}

# vim:set fenc=cp932 ts=2 sw=2 sts=2 et ai:
