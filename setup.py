#!/usr/bin/python
# coding: utf-8

# setup.py for python 2.7 on linux
# setup.py for python 3.5 on windows

# usage for linux:
# $ cd ~
# $ hg clone https://bitbucket.org/twabiko/my-dotfiles
# $ ./my-dotfiles/setup.py

# usage for windows:
# > cd %USERPROFILE%
# > hg clone https://bitbucket.org/twabiko/my-dotfiles
# > .\my-dotfiles\setup.py

import os.path
import json
import os
import sys
import subprocess

OS_TYPE_LINUX = 'linux'
OS_TYPE_WINDOWS = 'windows'

def is_linux():
    return os.name == 'posix'

def is_windows():
    return os.name == 'nt'

def os_type():
    if is_linux():
        return OS_TYPE_LINUX
    if is_windows():
        return OS_TYPE_WINDOWS
    assert(False)

def homedir():
    if is_linux():
        return os.environ['HOME']
    if is_windows():
        return os.environ['USERPROFILE']
    assert(False)

def load_inventory():
    path = os.path.join(homedir(), 'my-dotfiles', 'inventory.json')
    infile = open(path, 'rb')
    with infile:
        try:
            inventory = json.load(infile)
        except:
            raise SystemExit(e)
    return inventory

def is_valid_os(item, ostype):
    if not item.get(ostype, False):
        return False

    if not item[ostype]:
        return False

    return True

def my_mklink_windows(target, link):
    target_path = homedir()
    for part in target.split('/'):
        target_path = os.path.join(target_path, part)

    if os.path.isdir(target_path):
        subprocess.call(['mklink', '/D', link, target_path], shell=True)
    else:
        subprocess.call(['mklink', link, target_path], shell=True)

def my_mklink_linux(target, link):
    os.symlink(target, link)

def my_mklink(target, link):
    log('  ==> link: %s --> target: %s' % (link, target))
    if is_linux():
        return my_mklink_linux(target, link)
    elif is_windows():
        return my_mklink_windows(target, link)
    assert(False)

def process_item_pre(item):
    if item.get('pre', False):
        for shell in item['pre']:
            log('  ==> executing: %s' % (shell))
            subprocess.call(shell, shell=True)

def process_item_mode(item):
    if item.get('mode', False):
        if is_linux():
                mode = int(item['mode'], 8)
                os.chmod(item['link'], mode)
        elif is_windows():
            assert(False)
        else:
            assert(False)

def log(msg):
    print('%s' % (msg))

def process_item(item):
    log('processing item: %s' % (item['link']))

    if not is_valid_os(item, os_type()):
        log('  => skipped. (reason: platform mismatch)')
        log('')
        return

    if not os.path.exists(item['link']):
        process_item_pre(item)
        my_mklink(item['target'], item['link'])
        process_item_mode(item)
        log('  => done!')
        log('')
    else:
        log('  => skipped. (reason: path exists)')
        log('')

def process_inventory(inventory):
    for item in inventory:
        process_item(item)

def main():
    os.chdir(homedir())
    inventory = load_inventory()
    process_inventory(inventory)

if __name__ == '__main__':
    main()
