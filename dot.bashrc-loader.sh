# .bashrc-loader.sh

# usage
# append following line into your .bashrc file.
# pushd ~ >/dev/null; source .bashrc-loader.sh; popd >/dev/null

if [ -e ~/.bashrc.d ]; then
    for f in $(find .bashrc.d/ | grep \.sh$); do
        . $f
    done
fi
