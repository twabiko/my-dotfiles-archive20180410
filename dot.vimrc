" .vimrc

" ランタイムパス設定
" Linux は ~/.vim/ が Windows は ~/vimfiles/ がランタイムパスである。
" しかし、分けることによるメリットはあまり感じられない。
" むしろ、分けてしまうと Windows, Linux で設定を合わせるときに不便となった。
" ランタイムパスの OS環境デフォルト値は vimfiles の方が多いように見える。
" そこで、 vimfiles で統一することにした。
set runtimepath+=~/vimfiles

" フォント設定
source $HOME/vimfiles/vimrc/fonts.vim

" コマンド設定
source $HOME/vimfiles/vimrc/commands.vim

" 画面表示設定
source $HOME/vimfiles/vimrc/tab.vim

" IM設定
source $HOME/vimfiles/vimrc/im.vim

" 一時ファイル設定
source $HOME/vimfiles/vimrc/tmp.vim

" GUI設定
source $HOME/vimfiles/vimrc/gui.vim

" ファイルを開いたときの設定
source $HOME/vimfiles/vimrc/open.vim

" ファイル編集時の設定
source $HOME/vimfiles/vimrc/edit.vim

" プラグイン設定
source $HOME/vimfiles/vimrc/plugins.vim

" 関数定義
source $HOME/vimfiles/vimrc/functions.vim

" ToDo編集用定義(function、abbrを含む)
source $HOME/vimfiles/vimrc/todo.vim

" vim:ts=4 sw=4 sts=4 et:
