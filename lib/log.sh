#!bash

# log.sh

declare _log_facility=""

function echo_err {
  echo "$1" >&2
}

function confess {
    local mess max i line src

    echo test
    mess="$1"
    max=$((${#BASH_LINENO[@]}-1))

    echo hogehoge
    line=${BASH_LINENO[0]}
    src=${BASH_SOURCE[1]}
    echo_err "$mess at $src line $line"

    i=1
    while [ $i -lt $max ]; do
      echo hoge
      line=${BASH_LINENO[$i]}
      src=${BASH_SOURCE[$(($i + 1))]}
      echo_err "	${FUNCNAME[$i]}() called at $src line $line"
      i=$(($i+1))
    done

    exit 1
}

function log_get_facility {
    echo ${_log_facility:-user}
}

function log_set_facility {
    syslog_facility="$1"
}

function log {
    local pri="$(log_get_facility).${1:-notice}"
    local message="$2"
    logger -p $pri "$message"
}

function log_emerg {
    log "emerg" "$1"
}

function log_alert {
    log "alert" "$1"
}

function log_crit {
    log "crit" "$1"
}

function log_err {
    log "err" "$1"
}

function log_warning {
    log "warning" "$1"
}

function log_notice {
    log "notice" "$1"
}

function log_info {
    log "info" "$1"
}

function log_debug {
    log "debug" "$1"
}

